package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.enumerated.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_session")
public class Session extends AbstractUserOwnerModel {

    @NotNull
    @Column(nullable = false, name = "date")
    private Date date = new Date();

    @NotNull
    @Column(nullable = false, name = "role")
    private Role role = Role.USUAL;

}
