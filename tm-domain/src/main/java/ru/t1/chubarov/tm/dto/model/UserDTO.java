package ru.t1.chubarov.tm.dto.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public final class UserDTO extends AbstractModelDTO {

    @Nullable
    @Column(name = "login")
    private String login;

    @Nullable
    @Column(name = "password")
    private String passwordHash;

    @Nullable
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(nullable = false, name = "role")
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false, name = "locked")
    private Boolean locked = false;

    @NotNull
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(@NotNull Boolean locked) {
        this.locked = locked;
    }

    @Nullable
    public String getLogin() {
        return login;
    }

    @Nullable
    public String getPasswordHash() {
        return passwordHash;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    @Nullable
    public String getFirstName() {
        return firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    @Nullable
    public String getMiddleName() {
        return middleName;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setLogin(@Nullable String login) {
        this.login = login;
    }

    public void setPasswordHash(@Nullable String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    public void setFirstName(@Nullable String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(@Nullable String lastName) {
        this.lastName = lastName;
    }

    public void setMiddleName(@Nullable String middleName) {
        this.middleName = middleName;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

}
