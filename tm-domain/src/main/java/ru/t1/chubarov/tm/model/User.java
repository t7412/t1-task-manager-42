package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public class User extends AbstractUserOwnerModel {

    @Nullable
    @Column(name = "login")
    private String login;

    @Nullable
    @Column(name = "password")
    private String passwordHash;

    @Nullable
    @Column(name = "email")
    private String email;

    @Nullable
    @Column(name = "first_name")
    private String firstName;

    @Nullable
    @Column(name = "last_name")
    private String lastName;

    @Nullable
    @Column(name = "middle_name")
    private String middleName;

    @NotNull
    @Column(nullable = false, name = "role")
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false, name = "locked")
    private Boolean locked = false;

    @NotNull
    @OneToMany
    @JoinColumn(name = "user_id")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany
    @JoinColumn(name = "user_id")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany
    @JoinColumn(name = "user_id")
    private List<Session> sessions = new ArrayList<>();

}
