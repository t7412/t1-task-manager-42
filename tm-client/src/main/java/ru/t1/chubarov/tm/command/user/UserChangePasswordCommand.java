package ru.t1.chubarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.dto.request.UserChangePasswordRequest;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "change-user-password";
    @NotNull
    private final String DESCRIPTION = "Change password of current user.";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("[ENTER NEW PASSWORD]");
        @NotNull final String password = TerminalUtil.nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);
        getUserEndpoint().changeUserPassword(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
