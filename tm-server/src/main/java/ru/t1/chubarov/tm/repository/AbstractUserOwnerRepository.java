package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.DBConstants;
import ru.t1.chubarov.tm.api.repository.IUserOwnerRepository;
import ru.t1.chubarov.tm.dto.model.AbstractUserOwnerModelDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnerRepository<M extends AbstractUserOwnerModelDTO> extends AbstractRepository<M> implements IUserOwnerRepository<M> {

    @NotNull
    private final String USER_ID_COLUMN = DBConstants.COLUMN_USER_ID;

    public AbstractUserOwnerRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @NotNull final M model) throws Exception {
        if (userId == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @NotNull final Comparator comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        if (comparator != null) sql = String.format(sql + " ORDER BY %s", getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? AND %s = ? LIMIT 1", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    public M remove(@Nullable final String userId, @NotNull final M model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ? and %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE %s = ?", getTableName(), USER_ID_COLUMN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (userId == null || index < 0) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        @Nullable final List<M> models = findAll(userId);
        for (M model : models) {
            remove(model);
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception {
        return findOneById(userId, id) != null;
    }

}
