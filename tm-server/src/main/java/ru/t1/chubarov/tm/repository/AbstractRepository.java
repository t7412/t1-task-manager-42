package ru.t1.chubarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.DBConstants;
import ru.t1.chubarov.tm.api.repository.IRepository;
import ru.t1.chubarov.tm.comparator.CreatedComparator;
import ru.t1.chubarov.tm.comparator.StatusComparator;
import ru.t1.chubarov.tm.dto.model.AbstractModelDTO;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModelDTO> implements IRepository<M> {

    @NotNull
    public final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }


    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return DBConstants.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return DBConstants.COLUMN_STATUS;
        else return DBConstants.COLUMN_NAME;
    }

    @NotNull
    public abstract M fetch(@NotNull final ResultSet row) throws Exception;

    @NotNull
    public abstract M add(@NotNull final M model) throws Exception;

    @Override
    @NotNull
    public Collection<M> add(@NotNull final Collection<M> models) throws Exception {
        @NotNull List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }


    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql;
        if (comparator == null) sql = String.format("SELECT * FROM %s", getTableName());
        else sql = String.format("SELECT * FROM %s ORDER BY %s", getTableName(), getSortType(comparator));
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                result.add(fetch(resultSet));
            }
        }
        return result;
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE id = ? LIMIT 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final Integer index) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s LIMIT ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) {
                resultSet.next();
            }
            return fetch(resultSet);
        }
    }

    @Override
    public void clear() throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @NotNull
    @Override
    public M remove(@NotNull final M model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) throws Exception {
        clear();
        return add(models);
    }

}
