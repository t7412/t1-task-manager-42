package ru.t1.chubarov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectRepository {

    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status})")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void add(@NotNull final ProjectDTO project);


    @Insert("INSERT INTO tm_project (id, name, created, description, user_id, status)" +
            " VALUES (#{id}, #{name}, #{created}, #{description}, #{userId}, #{status})")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void addByUser(
            @NotNull final ProjectDTO model,
            @Param("userId2") @Nullable String userId2
    );

    @NotNull
    @Select("SELECT * FROM tm_project")
    List<ProjectDTO> findAll() throws Exception;

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    List<ProjectDTO> findAllByUser(@Nullable @Param("userId") String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} ORDER BY #{name}")
    @Results(value = {@Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllByUserSort(@Nullable @Param("userId") String userId, @NotNull @Param("name") String name) throws Exception;

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findOneById(@Nullable @Param("userId") String userId, @Nullable @Param("id") String id) throws Exception;

    @Nullable
    @Select("SELECT * FROM tm_project WHERE user_id = #{userId} LIMIT #{index}")
    @Results(value = {@Result(property = "userId", column = "user_id")
            , @Result(property = "index", column = "index")})
    ProjectDTO findOneByIndex(@Nullable @Param("userId") String userId, @Nullable @Param("index") Integer index) throws Exception;

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
            , @Result(property = "id", column = "id")
    })
    void remove(@NotNull final ProjectDTO model) throws Exception;

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
            , @Result(property = "id", column = "id")
    })
    void removeOneById(@Nullable @Param("userId") String userId, @Nullable @Param("id") String id) throws Exception;

    @Delete("DELETE FROM tm_project WHERE user_id = #{userId}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    void removeAllbyUser(@Nullable @Param("userId") String userId) throws Exception;

    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId}")
    int getSizeByUser(@Nullable @Param("userId") String userId) throws Exception;

    @Select("SELECT COUNT(*) FROM tm_project")
    int getSize() throws Exception;

    @Update("UPDATE tm_project SET name = #{name}, created = #{created}, description = #{description}, user_id = #{userId}, status = #{status} WHERE id = #{id}")
    @Results(value = {@Result(property = "userId", column = "user_id")})
    void update(@NotNull ProjectDTO project) throws Exception;

    @Delete("DELETE FROM tm_project;")
    void clear();

    @Select("SELECT COUNT(*) FROM tm_project WHERE user_id = #{userId} AND id = #{id}")
    int existsById(@Nullable @Param("userId") String userId, @NotNull @Param("id") String id) throws Exception;

}
