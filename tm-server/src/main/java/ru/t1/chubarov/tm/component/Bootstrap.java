package ru.t1.chubarov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.*;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.*;
import ru.t1.chubarov.tm.api.service.*;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enpoint.*;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.exception.system.CommandNotSupportedException;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;
import ru.t1.chubarov.tm.dto.model.UserDTO;
import ru.t1.chubarov.tm.repository.*;
import ru.t1.chubarov.tm.service.*;
import ru.t1.chubarov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.chubarov.tm.command";

    @NotNull
    public static final String SERVER_HOST = "server.host";

    @NotNull
    public static final String SERVER_PORT = "server.port";

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(projectService, taskService, propertyService, connectionService);

    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @Nullable
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() throws Exception {

        @NotNull final UserDTO test = userService.create("test", "test", "test@emal.ru");
        @NotNull final UserDTO user = userService.create("user", "user", "user@emal.ru");
        @NotNull final UserDTO admin = userService.create("admin", "admin", Role.ADMIN);
        userService.updateUser(admin.getId(), "AdminFirstName", "AdminLastName", "AdminMiddleName");

        projectService.add(test.getId(), new ProjectDTO("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(user.getId(), new ProjectDTO("ALFA PROJECT", Status.NOT_STARTED));
        projectService.add(user.getId(), new ProjectDTO("BETA PROJECT", Status.IN_PROGRESS));
        projectService.add(user.getId(), new ProjectDTO("DELTA PROJECT", Status.COMPLETED));
        projectService.create(admin.getId(), "DELTA ADMIN PROJECT", "PRJ ADMIN DESC");
        projectService.create(test.getId(), "BETA TEST PROJECT", "PRJ TEST DESC");
        taskService.create(test.getId(), "FIRST TASK", "FIRST DESCRIPTION");
        taskService.create(test.getId(), "SECOND TASK", "SECOND DESCRIPTION");
    }

    {
        regestry(userEndpoint);
        regestry(domainEndpoint);
        regestry(taskEndpoint);
        regestry(projectEndpoint);
        regestry(systemEndpoint);
        regestry(authEndpoint);
    }

    public void regestry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "6060";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start() throws Exception {
        initPID();
        //backup.start();
        initDemoData();
        loggerService.info("** WELCOME TO TASK SERVER MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                prepareShutdown();
            }
        });
    }

    private void prepareShutdown() {
        loggerService.info("** TASK SERVER MANAGER IS SHUTTING DOWN **");
        //backup.stop();
    }

    private void processCommand(@Nullable final String command) throws AbstractException, IOException {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) throws AbstractException, IOException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    @Nullable
    @Override
    public IProjectTaskService getProjectTaskService() {
        return null;
    }
}