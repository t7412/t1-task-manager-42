package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.ITaskRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.enumerated.TaskSort;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.dto.model.TaskDTO;
import ru.t1.chubarov.tm.service.ConnectionService;
import ru.t1.chubarov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String userIdFirst = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecond = UUID.randomUUID().toString();

    @NotNull
    private final String taskProjectId = UUID.randomUUID().toString();

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    final SqlSession sqlSession = connectionService.getSqlSession();
    @NotNull
    final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);

    @SneakyThrows
    @Before
    public void initRepository() {
        System.out.println("Task. Start Before.");
        taskList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("task_" + i);
            task.setProjectId(taskProjectId);
            task.setDescription("task description_" + i);
            if (i <= 5) task.setUserId(userIdFirst);
            else task.setUserId(userIdSecond);
            taskRepository.add(task);
            taskList.add(task);
        }
        sqlSession.commit();
    }

    @After
    public void finish() throws Exception {
        System.out.println("Start After.");
        taskList.clear();
        taskRepository.removeAll(userIdFirst);
        taskRepository.removeAll(userIdSecond);
        taskRepository.clear();
        sqlSession.commit();
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Task Description";
        @NotNull final TaskDTO task = new TaskDTO();
        @NotNull final String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userIdFirst);
        taskRepository.add(task);
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final TaskDTO actualtask = taskRepository.findOneById(userIdFirst, taskId);
        Assert.assertNotNull(actualtask);
        Assert.assertEquals(userIdFirst, actualtask.getUserId());
        Assert.assertEquals(name, actualtask.getName());
        Assert.assertEquals(description, actualtask.getDescription());
    }

    @SneakyThrows
    @Test
    public void testAddUserNegative() {
        @NotNull final String userId = null;
        @NotNull final String name = "Test Task";
        @NotNull final String description = "Test Task Description";
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveAllUser() {
        taskRepository.removeAll(userIdFirst);
        Assert.assertEquals(5, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testFindSort() {
        @NotNull final TaskSort sort = TaskSort.toSort("BY_NAME");
        @NotNull final List<TaskDTO> actualTaskList = taskRepository.findAllByUserSort(userIdFirst, "name");
        Assert.assertEquals(5, actualTaskList.size());

    }

    @Test
    public void testFindAllUser() throws Exception {
        @NotNull final List<TaskDTO> actualTaskList = taskRepository.findAllByUser(userIdFirst);
        Assert.assertEquals(5, actualTaskList.size());
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final String name = "Test Task";
        @NotNull final TaskDTO task = new TaskDTO();
        @NotNull final String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName(name);
        task.setUserId(userIdFirst);
        taskRepository.add(task);
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final TaskDTO actualTask = taskRepository.findOneById(userIdFirst, taskId);
        Assert.assertNotNull(actualTask);
        @NotNull final String taskId2 = actualTask.getId();
        Assert.assertNotNull(taskId2);
        Assert.assertEquals(taskId2, taskRepository.findOneById(userIdFirst, taskId2).getId());
    }

    @SneakyThrows
    @Test
    public void testFindByProjectId() {
        @Nullable final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userIdFirst, taskProjectId);
        Assert.assertEquals(5, tasks.size());
        Assert.assertEquals(0, taskRepository.findAllByProjectId(userIdFirst, "fail_roject_id").size());
    }

    @SneakyThrows
    @Test
    public void testRemoveOne() {
        taskRepository.remove(taskList.get(1));
        Assert.assertEquals(9, taskRepository.getSize());
        Assert.assertEquals(4, taskRepository.getSizeByName(userIdFirst));
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        int numberOfTask = NUMBER_OF_ENTRIES + 1;
        @NotNull final TaskDTO task = new TaskDTO();
        @NotNull final String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName("Test Task");
        task.setUserId(userIdFirst);
        taskRepository.add(task);
        taskRepository.removeOneById(userIdFirst, "fail_test_id");
        Assert.assertEquals(numberOfTask, taskRepository.getSize());
        @NotNull final TaskDTO actualTask = taskRepository.findOneById(userIdFirst, taskId);
        Assert.assertNotNull(actualTask);
        taskRepository.removeOneById(userIdFirst, actualTask.getId());
        Assert.assertEquals(numberOfTask - 1, taskRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testExistById() {
        @NotNull final String name = "Test Task";
        @NotNull final TaskDTO task = new TaskDTO();
        @NotNull final String taskId = UUID.randomUUID().toString();
        task.setId(taskId);
        task.setName(name);
        task.setUserId(userIdFirst);
        taskRepository.add(task);
        Assert.assertEquals(true, taskRepository.existsById(userIdFirst, taskId));
        Assert.assertEquals(false, taskRepository.existsById(userIdFirst, "1111"));
        Assert.assertEquals(false, taskRepository.existsById("userId-123-4", taskId));
    }

}
