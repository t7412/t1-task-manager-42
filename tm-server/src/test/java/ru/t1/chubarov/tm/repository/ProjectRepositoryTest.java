package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.IProjectRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.enumerated.ProjectSort;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.dto.model.ProjectDTO;
import ru.t1.chubarov.tm.service.ConnectionService;
import ru.t1.chubarov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String userIdFirst = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecond = UUID.randomUUID().toString();

    @NotNull
    private List<ProjectDTO> projectList;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    final SqlSession sqlSession = connectionService.getSqlSession();
    @NotNull
    final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);


    @SneakyThrows
    @Before
    public void initRepository() {
        System.out.println("Project. Start Before.");
        projectList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("project_" + i);
            project.setDescription("project description_" + i);
            if (i <= 5) project.setUserId(userIdFirst);
            else project.setUserId(userIdSecond);
            projectRepository.add(project);
            projectList.add(project);
        }
        sqlSession.commit();
    }

    @After
    public void finish() throws Exception {
        System.out.println("Project Start After.");
        projectList.clear();
        projectRepository.clear();
        sqlSession.commit();
        sqlSession.close();
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Project Description";
        @NotNull ProjectDTO project = new ProjectDTO();
        @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
        sqlSession.commit();
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        @Nullable final ProjectDTO actualproject = projectRepository.findOneById(userId, projectId);
        System.out.println("DEBUG: actualproject = " + actualproject);
        System.out.println("DEBUG: userId = " + userId);
        System.out.println("DEBUG: projectId = " + projectId);
        Assert.assertNotNull(actualproject);
        Assert.assertEquals(projectId, actualproject.getId());
        Assert.assertEquals(userId, actualproject.getUserId());
        Assert.assertEquals(name, actualproject.getName());
        Assert.assertEquals(description, actualproject.getDescription());
    }

    @SneakyThrows
    @Test
    public void testAddUserEmpty() {
        @NotNull final String userId = null;
        @NotNull final String name = "Test Project";
        @NotNull final String description = "Test Project Description";
        @NotNull final ProjectDTO model = new ProjectDTO();
        @NotNull String projectId = UUID.randomUUID().toString();
        model.setId(projectId);
        model.setName(name);
        model.setDescription(description);
        model.setUserId(userId);
        projectRepository.add(model);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveAllbyUser() {
        projectRepository.removeAllbyUser(userIdFirst);
        Assert.assertEquals(5, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testFindSort() {
        @NotNull final ProjectSort sort = ProjectSort.toSort("BY_NAME");
        @NotNull final List<ProjectDTO> actualProjectList = projectRepository.findAllByUserSort(userIdFirst, "name");
        Assert.assertEquals(5, actualProjectList.size());
        @NotNull final List<ProjectDTO> sortCreatedProjectList = projectRepository.findAllByUserSort(userIdFirst, "created");
        Assert.assertEquals(5, sortCreatedProjectList.size());
    }

    @Test
    public void testFindAllbyUser() throws Exception {
        @NotNull final List<ProjectDTO> actualProjectList = projectRepository.findAllByUser(userIdFirst);
        Assert.assertEquals(5, actualProjectList.size());
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull ProjectDTO project = new ProjectDTO();
        @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName(name);
        project.setUserId(userId);
        projectRepository.add(project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        Assert.assertNotNull(project);
        Assert.assertEquals(projectId, projectRepository.findOneById(userId, projectId).getId());
    }

    @SneakyThrows
    @Test
    public void testRemoveOne() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
        projectRepository.remove(projectList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projectRepository.getSize());
        Assert.assertEquals(4, projectRepository.getSizeByUser(userIdFirst));
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final ProjectDTO project = new ProjectDTO();
        @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName("Test Project");
        project.setUserId(userId);
        projectRepository.add(project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        @NotNull final ProjectDTO actualProject = projectRepository.findOneById(userId, projectId);
        Assert.assertNotNull(actualProject);
        projectRepository.removeOneById(userId, "fail_test_id");
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        projectRepository.removeOneById(userId, actualProject.getId());
        Assert.assertEquals(numberOfProject - 1, projectRepository.getSize());

        projectRepository.add(project);
        Assert.assertEquals(numberOfProject, projectRepository.getSize());
        projectRepository.removeOneById(userId, projectId);
        Assert.assertEquals(numberOfProject - 1, projectRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testExistById() {
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull final String name = "Test Project";
        @NotNull final ProjectDTO project = new ProjectDTO();
        @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName(name);
        project.setUserId(userId);
        projectRepository.add(project);
        Assert.assertEquals(true, projectRepository.existsById(userId, projectId) > 0);
        Assert.assertEquals(false, projectRepository.existsById(userId, "1111") > 0);
        Assert.assertEquals(false, projectRepository.existsById("userId-123-4", projectId) > 0);
    }
    @SneakyThrows
    @Test
    public void testUpdate() {
        int numberOfProject = NUMBER_OF_ENTRIES + 1;
        @NotNull final String userId = UUID.randomUUID().toString();
        @NotNull String name = "Test Project";
        @NotNull String description = "Test Project Description";
        @NotNull ProjectDTO project = new ProjectDTO();
        @NotNull String projectId = UUID.randomUUID().toString();
        project.setId(projectId);
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
        sqlSession.commit();
        Assert.assertEquals(numberOfProject, projectRepository.getSize());

        name = "NewProject";
        description = "Update Project desc";
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userIdFirst);
        projectRepository.update(project);
        @Nullable final ProjectDTO actualproject = projectRepository.findOneById(userIdFirst, projectId);
        Assert.assertNotNull(actualproject);
        Assert.assertEquals(projectId, actualproject.getId());
        Assert.assertEquals(userIdFirst, actualproject.getUserId());
        Assert.assertEquals(name, actualproject.getName());
        Assert.assertEquals(description, actualproject.getDescription());
    }

}
