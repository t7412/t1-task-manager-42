package ru.t1.chubarov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.api.service.ISessionService;
import ru.t1.chubarov.tm.exception.entity.ModelNotFoundException;
import ru.t1.chubarov.tm.exception.field.*;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.dto.model.SessionDTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionServiceTest {

    private static final int NUMBER_OF_ENTRIES = 2;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private final String firstSessionId = UUID.randomUUID().toString();

    @NotNull
    private final String userUserId = UUID.randomUUID().toString();

    @NotNull
    private final String userAdminId = UUID.randomUUID().toString();

    @Before
    public void initTest() throws Exception {
        sessionService = new SessionService(connectionService);
        sessionList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final SessionDTO session = new SessionDTO();
            session.setDate(new Date());
            if (i <= 1) {
                session.setId(firstSessionId);
                session.setUserId(userAdminId);
                sessionService.add(userAdminId, session);
            } else {
                session.setId(UUID.randomUUID().toString());
                session.setUserId(userUserId);
                sessionService.add(userUserId, session);
            }
            sessionList.add(session);
        }
    }

    @After
    public void finish() throws Exception {
        sessionService.removeAll(userAdminId, sessionList);
        sessionService.removeAll(userUserId, sessionList);
    }

    @Test
    public void testSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionService.getSize());
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        @NotNull final SessionDTO testSession1 =  new SessionDTO();
        @NotNull final SessionDTO testSession2=  new SessionDTO();
        testSession1.setUserId(userUserId);
        testSession2.setUserId(userUserId);
        sessionService.add(userUserId, testSession1);
        sessionService.add(userUserId, testSession2);
        sessionList.add(testSession1);
        sessionList.add(testSession2);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 2, sessionService.getSize());
    }

    @Test
    public void testCreateNegative() {
        @Nullable final SessionDTO session = null;
        Assert.assertThrows(ModelNotFoundException.class, () -> sessionService.add(userUserId, session));
    }

    @SneakyThrows
    @Test
    public void testExistsById() {
        Assert.assertTrue(sessionService.existsById(userAdminId,firstSessionId));
        Assert.assertFalse(sessionService.existsById(userUserId,firstSessionId));
        Assert.assertFalse(sessionService.existsById(userUserId,""));
        Assert.assertFalse(sessionService.existsById(userUserId,null));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById("",firstSessionId));
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.existsById(null,firstSessionId));
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        @Nullable final String userId = sessionList.get(0).getUserId();
        Assert.assertEquals(firstSessionId, sessionService.findOneById(userId, firstSessionId).getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> sessionService.findOneById("", firstSessionId).getId());
    }

    @Test
    public void testFindAll() throws Exception {
        @Nullable final List<SessionDTO> actualSessionList = sessionService.findAll();
        Assert.assertEquals(2, actualSessionList.size());
        @Nullable final List<SessionDTO> userSessionList = sessionService.findAll(userUserId);
        Assert.assertEquals(1, userSessionList.size());
    }

    @SneakyThrows
    @Test
    public void testRemove() {
        sessionService.remove(userUserId, sessionList.get(1));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

    @Test
    public void testRemoveAll() throws Exception {
        sessionService.removeAll(userAdminId, sessionList);
        sessionService.removeAll(userUserId, sessionList);
        Assert.assertEquals(0, sessionService.getSize(userAdminId));
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        sessionService.removeOneById(userUserId, sessionList.get(1).getId());
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionService.getSize());
    }

}
